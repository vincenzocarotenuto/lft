import java.io.*; 
import java.util.*;

public class Lexer {

    public static int line = 1;
    private char peek = ' ';
    
    private void readch(BufferedReader br) {
        try {
            peek = (char) br.read();
        } catch (IOException exc) {
            peek = (char) -1; // ERROR
        }
    }

    public Token lexical_scan(BufferedReader br) {
        while (peek == ' ' || peek == '\t' || peek == '\n'  || peek == '\r') {
            if (peek == '\n') line++;
            readch(br);
        }
        
        switch (peek) {
			
			// ... gestire i casi di (, ), +, -, *, /, ; ... //
			
            case '!':
                peek = ' ';
                return Token.not;
				
			case '(':
				peek=' ';
				return Token.lpt;
				
			case ')':
				peek=' ';
				return Token.rpt;
				
			case '+':
				peek=' ';
				return Token.plus;	
				
			case '-':
				peek=' ';
				return Token.minus;
				
			case '*':
				peek=' ';
				return Token.mult;
			
            case '/':
                readch(br);
                if (peek == '/') { //commento //
                    do {
                        readch(br);
                    } while (peek != '\n' && peek != (char) -1); //fine linea
                    return lexical_scan(br);
                } else if (peek == '*') { //commento /*
                    do {
                        do {
                            readch(br);
							if(peek == (char)-1){
								System.err.println("Commento non chiuso");
								return null;
							}
                        } while (peek != '*');
                        readch(br);
                        if (peek == '/') {
                            peek = ' ';
                            return lexical_scan(br);
                        }
                    } while (peek != (char)-1);
                } else {
                    return Token.div;
                }
				
			case ';':
				peek=' ';
				return Token.semicolon;

            case '&':
                readch(br);
                if (peek == '&') {
                    peek = ' ';
                    return Word.and;
                } else {
                    System.err.println("Erroneous character"
                            + " after & : "  + peek );
                    return null;
                }
				
			
			// ... gestire i casi di ||, <, >, <=, >=, ==, <>, = ... //
				
				
			case '|':
                readch(br);
                if (peek == '|') 
				{
                    peek=' ';
                    return Word.or;
                } else {
					//Sicuramente c'è un errore se subito dopo c'è un carattere diverso da |
                    System.err.println("Erroneous character"
                            + " after | : "  + peek );
                    return null;
                }
				
			//dobbiamo distinguere > e >=
			case '>':
                readch(br);
                if (peek == '=') {
                    peek=' ';
                    return Word.ge;
                } else 
				{
                    return Word.gt;
                }
			
			//dobbiamo distinguere da <, <=, <>
			case '<':
                readch(br);
                if (peek == '=') {
                    peek=' ';
                    return Word.le;
                } else 
					if (peek=='>'){
					peek=' ';
                    return Word.ne;
                } else 
				{
					return Word.lt;
				} 
			
			case '=':
				readch(br);
				if (peek == '='){ // ==
					peek=' ';
					return Word.eq;
				}
				else { // =
					return Token.assign;
				}	


          
            case (char)-1:
                return new Token(Tag.EOF);

            default:
			
				// ... gestire il caso degli identificatori e delle parole chiave //
				
				
                if (Character.isLetter(peek)) {
					int state=0;
					int i=0;
					String s="";
					while((Character.isLetter(peek) || Character.isDigit(peek) || peek=='_')&& peek!=(char)-1  && peek!=' '){
						s+=peek;
						readch(br);
					}
					if (s.equals("if"))return Word.iftok;
					else if (s.equals("then"))return Word.then;
					else if (s.equals("else"))return Word.elsetok;
					else if (s.equals("for"))return Word.fortok;
					else if (s.equals("do"))return Word.dotok;
					else if (s.equals("print"))return Word.print;
					else if (s.equals("begin"))return Word.begin;
					else if (s.equals("read"))return Word.read;
					else if (s.equals("end"))return Word.end;
					else {	
							while(state>=0 && i<s.length()){
							char ch=s.charAt(i++);
							switch(state){
								case 0:
									if ((ch>='A' && ch<='Z')||(ch>='a' && ch<='z'))
										state=1;
									else if(ch=='_')
										state=2;
									else
										state=-1;
									break;
								
								case 1:
									if ((ch=='_')||(ch>='A' && ch<='Z')||(ch>='a' && ch<='z')||(ch>='0' && ch<='9')){
										state=1;
									}
									break;
								case 2:if (ch=='_') state=2;
										else if((ch>='A' && ch<='Z')||(ch>='a' && ch<='z')||(ch>='0' && ch<='9')) state=1;
									break;
							}
						}
						if (state==1) 
							return new Word(Tag.ID,s);
						else{System.err.println("Erroneous character: " 
                                + peek );
								return null;
						}
					}
					
				// ... gestire il caso dei numeri ... //

                } else if (Character.isDigit(peek)) {
					String s="";
					while(Character.isDigit(peek)){
						s+=peek;
						readch(br);
						if (Character.isLetter(peek)||peek=='_'){
							System.out.println("Identificatore non valido!!");
							return null;
						}
					}
					return new NumberTok(Tag.NUM, Integer.parseInt(s));

                } else {
                        System.err.println("Erroneous character: " 
                                + peek );
                        return null;
                }
         }
    }
		
    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String path = "file.txt"; // il percorso del file da leggere
        try {
            BufferedReader br = new BufferedReader(new FileReader("file.txt"));
            Token tok;
            do {
                tok = lex.lexical_scan(br);
                System.out.println("Scan: " + tok);
            } while (tok.tag != Tag.EOF);
            br.close();
        } catch (IOException e) {e.printStackTrace();}    
    }

}
