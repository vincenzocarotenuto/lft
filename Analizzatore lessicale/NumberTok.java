public class NumberTok extends Token {
	int num;
    public NumberTok(int tag, int i) { 
		super(tag); 
		num=i; 
	}
	//Lexem=lessema
    public String toString() { 
		return "<" + tag + ", " + num + ">"; 
	}
}
