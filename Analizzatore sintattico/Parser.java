import java.io.*;

public class Parser {
    private Lexer lex;
    private BufferedReader pbr;
    private Token look;
    public Parser(Lexer l, BufferedReader br) {
        lex = l;
        pbr = br;
        move();
    }
	/*
	* Il metodo cattura e stampa il token in esame
	*
	*/
    void move() {
        look = lex.lexical_scan(pbr);
        System.out.println("token = " + look);
    }
	
	/*
	* Solleva un'eccezione in caso di errore informando sulla linea in cui si è verificato e il tipo di errore
	*
	*/
	
    void error(String s) {
	throw new Error("near line " + lex.line + ": " + s);
    }
	
	/*
	*
	*
	*/
	
    void match(int t) {
	if (look.tag == t) {
	    if (look.tag != Tag.EOF) move();
	} else error("syntax error");
    }
	
	/*
	* prog = statlist EOF
	*
	*/
	
	public void prog() {
        if (look.tag == Tag.PRINT || look.tag == Tag.READ || look.tag == Tag.IF || look.tag == Tag.FOR || look.tag == Tag.ID || look.tag == Tag.BEGIN) {
            statlist();
            if (look.tag == Tag.EOF) {
                match(Tag.EOF);
            } else {
                error("Syntax error in prog. Expected '" + Tag.EOF + "', instead read " + look.tag);
            }
        } else {
            error("Syntax error in prog " + look.tag);
        }
    }

	/*
	* stat ::= ID = expr | 
		print ( expr ) | 
		read ( ID ) | 
		if bexpr then stat | 
		if bexpr then stat else stat  | 
		for ( ID =expr ;bexpr) do stat | 
		begin statlist end 
	*
	*/
	
    private void stat() {
        switch (look.tag) {
            case Tag.PRINT:
                match(Tag.PRINT);
                if (look.tag == '(') {
                    match('(');
                } else {
                    error("Syntax error in stat. Expected '(', instead read '" + _printTag(look) + "'");
                }
                expr();
                if (look.tag == ')') {
                    match(')');
                } else {
                    error("Syntax error in stat. Expected ')', instead read '" + _printTag(look) + "'");
                }
                break;
            case Tag.READ:
                match(Tag.READ);
                if (look.tag == '(') {
                    match('(');
                } else {
                    error("Syntax error in stat. Expected '(', instead read '" + _printTag(look) + "'");
                }
                if(look.tag == Tag.ID)
                match(Tag.ID);
                else {
                     error("Syntax error in stat. Expected a valid id, instead read '" + _printTag(look) + "'");
                }
                if (look.tag == ')') {
                    match(')');
                } else {
                    error("Syntax error in stat. Expected ')', instead read '" + _printTag(look) + "'");
                }
                break;
            case Tag.IF:
                match(Tag.IF);
                bexpr();
				if (look.tag == Tag.THEN) {
					match(Tag.THEN);
                } else {
                    error("Syntax error in stat. Expected 'then', instead read '" + _printTag(look) + "'");
                }
                stat();
                stat_p();
				if (look.tag == (int)';') {
					match(';');
                } else {
                    error("Syntax error in stat. Expected ';' at the end of body of if-then-else construct, instead read '" + _printTag(look) + "'");
                }
                break;
            case Tag.FOR:
                match(Tag.FOR);
                if (look.tag == '(') {
                    match('(');
                } else {
                    error("Syntax error in stat. Expected '(', instead read '" + _printTag(look) + "'");
                }
				match(Tag.ID);
				if (look.tag == '=') {
                    match('=');
                } else {
                    error("Syntax error in stat. Expected '=', instead read '" + _printTag(look) + "'");
                }
                expr();
                if (look.tag == ';') {
                    match(';');
                } else {
                    error("Syntax error in stat. Expected ';', instead read '" + _printTag(look) + "'");
                }
                bexpr();
				if (look.tag == ')') {
                    match(')');
                } else {
                    error("Syntax error in stat. Expected ')', instead read '" + _printTag(look) + "'");
                }
				if (look.tag == Tag.DO) {
                    match(Tag.DO);
                } else {
                    error("Syntax error in stat. Expected 'do', instead read '" + _printTag(look) + "'");
                }
				stat();
                break;
            case Tag.BEGIN:
                match(Tag.BEGIN);
                statlist();
                if (look.tag == Tag.END) {
                    match(Tag.END);
                } else {
                    error("Syntax error in stat. Expected '}', instead read '" + _printTag(look) + "'");
                }
                break;
            case Tag.ID:
                match(Tag.ID);
                if (look.tag == '=') {
                    match('=');
                } else {
                    error("Syntax error in stat. Expected " + _printTag(Word.assign) + ", instead read '" + _printTag(look) + "'");
                }
                expr();
                break;
            default:
                error("Syntax error in stat " + look.tag);
                break;
        }
    }

   private void stat_p() {
        switch (look.tag) {
            case Tag.ELSE:
                match(Tag.ELSE);
                stat();
                break;
            case (int) ';':
                break;
            default:
                error("Syntax error in stat_p, Expected ELSE or ';'. " + look.tag);
                break;
        }
    }

	/*
	* statlist ::= stat statlistp
	*
	*/
	
    private void statlist() {
        switch (look.tag) {
            case Tag.PRINT:
            case Tag.READ:
            case Tag.IF:
            case Tag.FOR:
            case Tag.BEGIN:
            case Tag.ID:
                stat();
                statlist_p();
                break;
            default:
                error("Syntax error in statlist " + look.tag);
                break;
        }
    }

	/*
	* statlistp ::= ; stat statlistp | ε 
	*
	*/
	
    private void statlist_p() {
        switch (look.tag) {
            case ';':
                match(';');
                stat();
                statlist_p();
                break;
            case Tag.END:
            case Tag.EOF:
                break;
            default:
                error("Syntax error in statlist_p " + look.tag);
                break;
        }
    }

	/*
	* bexpr ::= expr RELOP expr 
	*
	*/
	
    private void bexpr() {
        if (look.tag == Tag.ID || look.tag == Tag.NUM || look.tag == (int) '(') {
            expr();
            if (look.tag == Tag.RELOP) {
                match(Tag.RELOP);
            } else {
                error("Syntax error in bexpr. Expected '<=, >=, ==, < or >', instead read '" + _printTag(look) + "'");
            }
            expr();
        } else {
            error("Syntax error in bexpr " + look.tag);
        }
    }

	/*
	* expr ::= term exprp
	*
	*/
	
    private void expr() {
        if (look.tag == Tag.ID || look.tag == Tag.NUM || look.tag == (int) '(') {
            term();
            exprp();
        } else {
            error("Syntax error in expr " + look.tag);
        }
    }

	/*
	* exprp ::= + term exprp | - term exprp | ε 
	*
	*/
	
    private void exprp() {
        switch (look.tag) {
            case '+':
                match('+');
                term();
                exprp();
                break;
            case '-':
                match('-');
                term();
                exprp();
                break;
            case ')':
            case ';':
            case Tag.THEN:
            case Tag.ELSE:
			case Tag.END:
            case Tag.RELOP:
            case Tag.EOF:
                break;
            default:
                error("Syntax error in exprp " + look.tag);
                break;
        }
    }

	/*
	* term ::= fact termp
	*
	*/
		 
    private void term() {
        if (look.tag == (int) '(' || look.tag == Tag.NUM || look.tag == Tag.ID) {
            fact();
            termp();
        } else {
            error("Syntax error in term " + look.tag);
        }
    }

	/*
	* termp ::= * fact termp | / fact termp | ε 
	*
	*/
	
    private void termp() {
        switch (look.tag) {
            case '*':
                match('*');
                fact();
                termp();
                break;
            case '/':
                match('/');
                fact();
                termp();
                break;
            case '+':
            case '-':
            case ')':
			case ';':
            case Tag.THEN:
            case Tag.ELSE:
			case Tag.END:
            case Tag.RELOP:
            case Tag.EOF:
                break;
            default:
                error("Syntax error in termp " + (look.tag));
                break;
        }

    }

	/*
	* fact ::= (expr) | NUM | ID 
	*
	*/
	
    private void fact() {
        switch (look.tag) {
            case Tag.NUM:
            case Tag.ID:
                match(look.tag);
                break;
            case '(':
                match('(');
                expr();
                if (look.tag == ')') {
                    match(')');
                } else {
                    error("Syntax error in fact. Expected ')', instead read '" + _printTag(look) + "'");
                }
                break;
            default:
                error("Syntax error in fact " + (look.tag));
                break;
        }
    }
		
    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String path = "file.txt"; // il percorso del file da leggere
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            Parser parser = new Parser(lex, br);
            parser.prog();
            System.out.println("Input OK");
            br.close();
        } catch (IOException e) {e.printStackTrace();}
    }
	
	private static String _printTag(Token t) {
        if (t.tag >= 0 && t.tag < 255) {
            return "" + (char) t.tag;
        } else if (t instanceof Word) {
            return ((Word) t).lexeme;
        } else if (t instanceof NumberTok) {
            return "" + ((NumberTok) t).num;
        } else {
            return "" + t.tag;
        }
    }
}