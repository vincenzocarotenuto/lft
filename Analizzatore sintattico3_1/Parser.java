import java.io.*;

public class Parser {
    private Lexer lex;
    private BufferedReader pbr;
    private Token look;
    public Parser(Lexer l, BufferedReader br) {
        lex = l;
        pbr = br;
        move();
    }
	
    void move() {
		//look= token che vogliamo esaminare
        look = lex.lexical_scan(pbr);
        System.out.println("token = " + look);
    }
	
    void error(String s) {
	throw new Error("near line " + lex.line + ": " + s);
    }

    void match(int t) {
	if (look.tag == t) {
	    if (look.tag != Tag.EOF) move();
	} else error("syntax error");
    }
	
	//<start> = <expr> + EOF
    public void start(){
		if (look.tag == (int)'(' || look.tag == Tag.NUM) {
			expr();
			match(Tag.EOF);
		}
		else{
			error("Syntax error in Start. Expected '(' or NUM, instead read '" + _printTag(look) + "'");
		}
    }

	//<expr> = <term> <exprp> 
    private void expr(){
		if (look.tag == (int)'(' || look.tag == Tag.NUM) {
			term();
			exprp();
		}
		else{
			error("Syntax error in expr. Expected '(' or NUM, instead read '" + _printTag(look) + "'");
		}
    }
	
	//<exprp> = + <term> <exprp> | - <term> <exprp> | epsilon
    private void exprp(){
		switch (look.tag) {
			case '+':	
				match((int)'+');
				term();
				exprp();
				break;
			case '-':	
				match((int)'-');
				term();
				exprp();
				break;
			case Tag.EOF: 	
			case ')':
				break;
			default:
				error("Syntax error in exprp. " + _printTag(look) + "'");
		}
	}

    private void term(){
		if (look.tag == (int)'(' || look.tag == Tag.NUM) {
			fact();
			termp();
		}else{
			error("Syntax error in term. Expected '(' or NUM, instead read '" + _printTag(look) + "'");
		}
    }
	//<termp> = * <fact> <termp> | / <fact> <termp> | epsilon
    private void termp() {
		switch (look.tag) {
			case '*':	
				match((int)'*');
				fact();
				termp();
				break;
			case '/':	
				match((int)'/');
				fact();
				termp();
				break;
			case Tag.EOF: 	
			case ')':
			case '+':
			case '-':
				break;
			default: 	
				error("Syntax error in termp. " + _printTag(look) + "'");
		}
    }
	//<fact>:= ( <expr> ) | NUM
    private void fact() {
		switch(look.tag) {
			case Tag.NUM:
				match(Tag.NUM);
				break;
			case '(':
				match((int)'(');
				expr();
				match((int)')');
				break;
			default: 
				error("Syntax error in fact. " + _printTag(look) + "'");
		}
    }
		
    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String path = "file.txt"; // il percorso del file da leggere
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            Parser parser = new Parser(lex, br);
            parser.start();
            System.out.println("Input OK");
            br.close();
        } catch (IOException e) {e.printStackTrace();}
    }
	
	private static String _printTag(Token t) {
        if (t.tag >= 0 && t.tag < 255) {
            return "" + (char) t.tag;
        } else if (t instanceof Word) {
            return ((Word) t).lexeme;
        } else if (t instanceof NumberTok) {
            return "" + ((NumberTok) t).num;
        } else {
            return "" + t.tag;
        }
    }
}