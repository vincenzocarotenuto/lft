import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExpressionTranslator {
	private Lexer lex; 
	private BufferedReader pbr; 
	private Token look;
	
	CodeGenerator code = new CodeGenerator();
	
	public ExpressionTranslator(Lexer l, BufferedReader br) { 
		lex = l; 
		pbr = br; 
		move(); 
	}

    void move() {
        try {
            look = lex.lexical_scan(pbr);
        } catch (Exception ex) {
            Logger.getLogger(
			ExpressionTranslator.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.err.println("token = " + look);
    }

    void error(String s) {
        throw new Error("near line " + lex.line + ": " + s);
    }

    void match(int t) {
        if (look.tag == t) {
            if (look.tag != Tag.EOF) {
                move();
            }
        } else {
            error("syntax error");
        }
    }

    public void prog() {
        if (look.tag == (int)'(' || look.tag == Tag.NUM || look.tag == Tag.PRINT) {
			match(Tag.PRINT);
			if (look.tag == '(') {
				match('(');
            } else {
				error("Syntax error in prog. Expected '(', instead read '" + _printTag(look) + "'");
			}
			expr(); 
			code.emit(OpCode.invokestatic,1); 
			if (look.tag == ')') {
				match(')');
            } else {
				error("Syntax error in prog. Expected ')', instead read '" + _printTag(look) + "'");
			}
			match(Tag.EOF); 
			try { 
				code.toJasmin(); 
			} 
			catch(java.io.IOException e) { 
				System.out.println("IO error\n"); 
			}; 
        } else {
            error("Syntax error in prog " + look.tag);
        }
    }
	
	public void expr() {
        if (look.tag == (int)'(' || look.tag == Tag.NUM) {
			term(); 
			exprp();
        } else {
            error("Syntax error in expr " + look.tag);
        }
    }

	private void exprp(){
		switch (look.tag) {
			case '+':	
				match('+'); 
				term(); 
				code.emit(OpCode.iadd); 
				exprp(); 
				break;
			case '-':	
				match((int)'-');
				term();
				code.emit(OpCode.isub); 
				exprp();
				break;
			case Tag.EOF: 	
			case ')':
				break;
			default:
				error("Syntax error in exprp. " + _printTag(look) + "'");
		}
	}
	
	private void term(){
		if (look.tag == (int)'(' || look.tag == Tag.NUM) {
			fact();
			termp();
		}else{
			error("Syntax error in term. Expected '(' or NUM, instead read '" + _printTag(look) + "'");
		}
    }
	
    private void termp(){
		switch (look.tag) {
			case '*':	
				match((int)'*');
				fact();
				code.emit(OpCode.imul); 
				termp();
				break;
			case '/':	
				match((int)'/');
				code.emit(OpCode.idiv);
				fact();
				termp();
				break;
			case Tag.EOF: 	
			case ')':
			case '+':
			case '-':
				break;
			default: 	
				error("Syntax error in termp. " + _printTag(look) + "'");
		}
    }
	
    private void fact() {
        switch (look.tag) {
            case Tag.NUM:
                code.emit(OpCode.ldc, ((NumberTok) look).num);
                match(look.tag);
                break;
            case '(':
                match('(');
                expr();
                if (look.tag == ')') {
                    match(')');
                } else {
                    error("Syntax error in fact. Expected ')', instead read '" + _printTag(look) + "'");
                }
                break;
            default:
                error("Syntax error in fact " + (look.tag));
                break;
		}
	}
	
    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String path = new File("file.txt").getAbsolutePath();
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            ExpressionTranslator parser = new ExpressionTranslator(lex, br);
            parser.prog();
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String _printTag(Token t) {
        if (t.tag >= 0 && t.tag < 255) {
            return "" + (char) t.tag;
        } else if (t instanceof Word) {
            return ((Word) t).lexeme;
        } else if (t instanceof NumberTok) {
            return "" + ((NumberTok) t).num;
        } else {
            return "" + t.tag;
        }
    }
}
