import java.io.*;

public class Translator {
	private Lexer lex; 
	private BufferedReader pbr; 
	private Token look;
	
	SymbolTable st = new SymbolTable(); 
	CodeGenerator code = new CodeGenerator(); 
	int count=0;
	
	public Translator(Lexer l, BufferedReader br) { 
		lex = l; 
		pbr = br; 
		move(); 
	}

    void move() {
        look = lex.lexical_scan(pbr);
        System.err.println("token = " + look);
    }

    void error(String s) {
        throw new Error("near line " + lex.line + ": " + s);
    }

    void match(int t) {
        if (look.tag == t) {
            if (look.tag != Tag.EOF) {
                move();
            }
        } else {
            error("syntax error");
        }
    }

	
	/*
	* prog = statlist EOF
	*
	*/
	
	public void prog() {
        if (look.tag == Tag.PRINT || look.tag == Tag.READ || look.tag == Tag.IF || look.tag == Tag.FOR || look.tag == Tag.ID || look.tag == Tag.BEGIN) {
            int lnext_prog = code.newLabel(); 
			statlist(lnext_prog); 
			code.emitLabel(lnext_prog); 
			match(Tag.EOF); 
			try { 
				code.toJasmin(); 
			} catch(java.io.IOException e) { 
				System.out.println("IO error\n"); 
			};
        } else {
            error("Syntax error in prog " + look.tag);
        }
    }

	/*
	* stat ::= ID = expr | 
	*	print ( expr ) | 
	*	read ( ID ) | 
	*	if bexpr then stat | 
	*	if bexpr then stat else stat  | 
	*	for (ID =expr ;bexpr) do stat | 
	*	begin statlist end 
	*
	*/
	
    private void stat(int stat_next) {
		int b_true, b_false, b_begin, statlist_next, m_val; 
		String id;
		Token m_token;
        switch (look.tag) {
			case Tag.PRINT: 
				match(Tag.PRINT); 
				match('('); 
				expr(); 
				code.emit(OpCode.invokestatic,1); 
				match(')'); 
				break; 
			case Tag.READ: 
				match(Tag.READ); 
				match('('); 
				if (look.tag==Tag.ID) { 
					int read_id_addr = st.lookupAddress(((Word)look).lexeme); 
					if (read_id_addr==-1) { 
						read_id_addr = count; st.insert(((Word)look).lexeme,count++); 
					} 
					match(Tag.ID); 
					match(')'); 
					code.emit(OpCode.invokestatic,0); code.emit(OpCode.istore,read_id_addr); 
				} else 
					error("Error in grammar (stat) after read '(' with " + look); 
				break;
          case Tag.IF:
                match(Tag.IF);
                bexpr();
				if (look.tag == Tag.THEN) {
					match(Tag.THEN);
                } else {
                    error("Syntax error in stat. Expected 'then', instead read '" + _printTag(look) + "'");
                }
                b_true= code.newLabel();
				b_false= code.newLabel();
                code.emit(OpCode.ifne, b_true);
				code.emit(OpCode.ldc, 0);				
                code.emit(OpCode.GOto, b_false);
				code.emitLabel(b_true);
				stat(stat_next);
				code.emit(OpCode.ldc, 1);
				code.emitLabel(b_false);
                stat_p(stat_next);
				if (look.tag == (int)';') {
					match(';');
                } else {
                    error("Syntax error in stat. Expected ';' at the end of body of if-then-else construct, instead read '" + _printTag(look) + "'");
                }
                break;
            case Tag.FOR:
                match(Tag.FOR);
                if (look.tag == '(') {
                    match('(');
                } else {
                    error("Syntax error in stat. Expected '(', instead read '" + _printTag(look) + "'");
                }
				
				//ID = <expr>
				m_token = look;
                match(Tag.ID);
                id = ((Word) m_token).lexeme;
				if (look.tag == '=') {
                    match('=');
                } else {
                    error("Syntax error in stat. Expected " + _printTag(Word.assign) + ", instead read '" + _printTag(look) + "'");
                }
                expr();
                m_val = st.lookupAddress(id);
                if (m_val == -1) {
                    code.emit(OpCode.istore, count);
                    st.insert(id, count++);
                } else {
                    code.emit(OpCode.istore, m_val);
                }
				
				if (look.tag == ';') {
                    match(';');
                } else {
                    error("Syntax error in stat. Expected ';', instead read '" + _printTag(look) + "'");
                }
				b_begin = code.newLabel();
                code.emitLabel(b_begin);
				b_false = stat_next;
				b_true = code.newLabel();
				bexpr();
				if (look.tag == ')') {
                    match(')');
                } else {
                    error("Syntax error in stat. Expected ')', instead read '" + _printTag(look) + "'");
                }
				if (look.tag == Tag.DO) {
                    match(Tag.DO);
                } else {
                    error("Syntax error in stat. Expected 'do', instead read '" + _printTag(look) + "'");
                }
				code.emit(OpCode.ifne, b_true);
				code.emit(OpCode.GOto, b_false);
				code.emitLabel(b_true);
				stat_next = b_begin;
				stat(stat_next);
				code.emit(OpCode.GOto, stat_next);
                break;
            case Tag.BEGIN:
                match(Tag.BEGIN);
				statlist_next = stat_next;
                statlist(statlist_next);
                if (look.tag == Tag.END) {
                    match(Tag.END);
                } else {
                    error("Syntax error in stat. Expected END, instead read '" + _printTag(look) + "'");
                }
                break;
            case Tag.ID:
				m_token = look;
                match(Tag.ID);
                id = ((Word) m_token).lexeme;
				if (look.tag == '=') {
                    match('=');
                } else {
                    error("Syntax error in stat. Expected " + _printTag(Word.assign) + ", instead read '" + _printTag(look) + "'");
                }
                expr();
                m_val = st.lookupAddress(id);
                if (m_val == -1) {
                    code.emit(OpCode.istore, count);
                    st.insert(id, count++);
                } else {
                    code.emit(OpCode.istore, m_val);
                }
                break;
            default:
                error("Syntax error in stat " + look.tag);
                break;
        }
    }

   private void stat_p(int stat_next) {
        switch (look.tag) {
            case Tag.ELSE:
                match(Tag.ELSE);
				int end_if = code.newLabel();
                code.emit(OpCode.ifne, end_if); //eq controlla se cima stack==1
                stat(stat_next);
				code.emitLabel(end_if);
                break;
            case (int) ';':
                break;
            default:
                error("Syntax error in stat_p. Expected ELSE or ';'. " + look.tag);
                break;
        }
    }

	/*
	* statlist ::= stat statlistp
	*
	*/
	
    private void statlist(int statlist_next) {
		int stat_next, statlist_p_next;
        switch (look.tag) {
            case Tag.PRINT:
            case Tag.READ:
            case Tag.IF:
            case Tag.FOR:
            case Tag.BEGIN:
            case Tag.ID:
				statlist_p_next = code.newLabel();
				stat_next = statlist_p_next;
                stat(stat_next);
                statlist_p(statlist_p_next);
                break;
            default:
                error("Syntax error in statlist " + look.tag);
                break;
        }
    }

	/*
	* statlistp1 ::= ; stat statlistp1 | ε 
	*
	*/
	
    private void statlist_p(int statlist_next) {
		int statlist_p_next, stat_next;
        switch (look.tag) {
            case ';':
				if(look.tag == (int)';'){
					match(';');
				} else {
					error("Syntax error in stalist_p. Expected ';', instead read '" + _printTag(look) + "'");
				}
				code.emitLabel(statlist_next);
				statlist_p_next = code.newLabel();
				stat_next = statlist_p_next; 
                stat(stat_next);
                statlist_p(statlist_p_next);
                break;
            case Tag.END:
            case Tag.EOF:
				code.emitLabel(statlist_next);
                break;
            default:
                error("Syntax error in statlist_p " + look.tag);
                break;
        }
    }

	/*
	* bexpr ::= expr RELOP expr 
	*
	*/
	
    private void bexpr() { 
        if (look.tag == Tag.ID || look.tag == Tag.NUM || look.tag == (int) '(') {
            expr();
            Token m_relop = look;
            if (look.tag == Tag.RELOP) {
                match(Tag.RELOP);
            } else {
                error("Syntax error in bexpr. Expected '<=, >=, ==, < or >', instead read '" + _printTag(look) + "'");
            }
            expr();
            int ltrue = code.newLabel();
            int lfalse = code.newLabel();
            if (((Word) m_relop).equals(Word.eq)) {
                code.emit(OpCode.if_icmpeq, ltrue);
            } else if (((Word) m_relop).equals(Word.lt)) {
                code.emit(OpCode.if_icmplt, ltrue);
            } else if (((Word) m_relop).equals(Word.le)) {
                code.emit(OpCode.if_icmple, ltrue);
            } else if (((Word) m_relop).equals(Word.gt)) {
                code.emit(OpCode.if_icmpgt, ltrue);
            } else if (((Word) m_relop).equals(Word.ge)) {
                code.emit(OpCode.if_icmpge, ltrue);
            } else if (((Word) m_relop).equals(Word.ne)) {
                code.emit(OpCode.if_icmpne, ltrue);
            } else {
                error("Errore del traduttore nel RELOP");
            }
            code.emit(OpCode.ldc, 0);
            code.emit(OpCode.GOto, lfalse);
            code.emitLabel(ltrue);
            code.emit(OpCode.ldc, 1);
            code.emitLabel(lfalse);

        } else {
            error("Syntax error in bexpr " + look.tag);
        }
    }
    
	
    private void expr() {
        if (look.tag == Tag.ID || look.tag == Tag.NUM || look.tag == (int) '(') {
            term();
            exprp();
        } else {
            error("Syntax error in expr " + look.tag);
        }
    }

    private void exprp() {
        switch (look.tag) {
            case '+':
                match('+');
                term();
                exprp(); 
                code.emit(OpCode.iadd);
                break;
            case '-':
                match('-');
                term();
                exprp();
                code.emit(OpCode.isub);
                break;
            case ')':
            case ';':
            case Tag.THEN:
            case Tag.ELSE:
			case Tag.END:
            case Tag.RELOP:
            case Tag.EOF:
                break;
            default:
                error("Syntax error in exprp " + look.tag);
                break;
        }
    }

    private void term() {
        if (look.tag == (int) '(' || look.tag == Tag.NUM || look.tag == Tag.ID) {
            fact();
            termp();
        } else {
            error("Syntax error in term " + look.tag);
        }
    }

    private void termp() {
        switch (look.tag) {
            case '*':
                match('*');
                fact();
                termp();
                code.emit(OpCode.imul);
                break;
            case '/':
                match('/');
                fact();
                termp();
                code.emit(OpCode.idiv);
                break;
            case '+':
            case '-':
            case ')':
			case ';':
            case Tag.THEN:
            case Tag.ELSE:
			case Tag.END:
            case Tag.RELOP:
            case Tag.EOF:
                break;
            default:
                error("Syntax error in termp " + (look.tag));
                break;
        }

    }

    private void fact() {
        switch (look.tag) {
            case Tag.NUM:
                code.emit(OpCode.ldc, ((NumberTok) look).num);
                match(look.tag);
                break;
            case Tag.ID:
                Token m_id = look;
                match(look.tag);
                code.emit(OpCode.iload, st.lookupAddress(((Word) m_id).lexeme));
                break;
            case '(':
                match('(');
                expr();
                if (look.tag == ')') {
                    match(')');
                } else {
                    error("Syntax error in fact. Expected ')', instead read '" + _printTag(look) + "'");
                }
                break;
            default:
                error("Syntax error in fact " + (look.tag));
                break;
        }
    }
		
	
    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String path = new File("file.txt").getAbsolutePath();
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            Translator parser = new Translator(lex, br);
            parser.prog();
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String _printTag(Token t) {
        if (t.tag >= 0 && t.tag < 255) {
            return "" + (char) t.tag;
        } else if (t instanceof Word) {
            return ((Word) t).lexeme;
        } else if (t instanceof NumberTok) {
            return "" + ((NumberTok) t).num;
        } else {
            return "" + t.tag;
        }
    }
}
