//Programma in cui l'input non contiene tre 0 consegutivi
public class Es1{
	
	public static void main(String[]args){
		if (args.length < 1) {
			System.out.println("Errore: occorre inserire almeno 1 argomenti!");
			System.exit(-1);
		}	
		System.out.println(scan(args[0])?"Ok":"No");
	}
	
	public static boolean scan(String s){
		int state=0;
		int i=0;
		while((state>=0)&&(i<s.length())){
			//ch prende valori i-esimi della stringa
			final char ch=s.charAt(i++);
			
			switch(state){
			case 0:
				//simulare le azioni dell'automa a q0
				if (ch=='0')
					state=1;
				else if (ch=='1')
					state=0;
				else 
					//quando un simbolo letto non ha senso viene assegnato  il 					//valore -1
					state=-1;
				break;	
			case 1:
				//simulare le azioni dell'automa a q1
				if (ch=='0')
					state=2;
				else if (ch=='1')
					state=0;
				else 
					//quando un simbolo letto non ha senso viene assegnato  il 					//valore -1
					state=-1;
				break;
			
			case 2:
				//simulare le azioni dell'automa a q0
				if (ch=='0')
					state=3;
				else if (ch=='1')
					state=0;
				else 
					//quando un simbolo letto non ha senso viene assegnato  il 					//valore -1
					state=-1;
				break;
			case 3:
				//simulare le azioni dell'automa a q0
				if (ch=='0' || ch=='1')
					state=3;
				else 
					//quando un simbolo letto non ha senso viene assegnato  il 					//valore -1
					state=-1;
				break;
			
			}
		}
		return state != 3;
	}
	
}