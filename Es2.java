public class Es2{
	
	public static void main(String[]args){
		if (args.length<1){
			System.out.println("Inserire almeno un carattere");
			System.exit(-1);
		}
		System.out.println(scan(args[0])?"Ok":"Identificatore non valido!\n");
	}
	
	public static boolean scan(String s){
		int state=0;
		int i=0;
		
		while(state>=0 && i<s.length()){
			char ch=s.charAt(i++);
			switch(state){
				case 0:
					if ((ch>='A' && ch<='Z')||(ch>='a' && ch<='z'))
						state=1;
					else if(ch=='_')
						state=2;
					else
						state=-1;
					break;
				
				case 1:
					if ((ch=='_')||(ch>='A' && ch<='Z')||(ch>='a' && ch<='z')||(ch>='0' && ch<='9')){
						state=1;
					}
					break;
				case 2:
					if (ch=='_') 
						state=2;
					else if((ch>='A' && ch<='Z')||(ch>='a' && ch<='z')||(ch>='0' && ch<='9')) 
						state=1;
					break;
			}
		}
		return state==1;
	}
	
}