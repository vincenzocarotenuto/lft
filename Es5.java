public class Es5{
	public static void main(String[]args){
		System.out.println(turn(args[0])?"ok":"No");
	}
	public static boolean turn(String s){
		int state=0;
		int i=0;
		while(state>=0 && i<s.length()){
			char ch=s.charAt(i++);
			switch(state){
				case	0:
					if (ch>='A' && ch<='K')
						state=2;
					else if(ch>='L' && ch<='Z')
						state=1;
					else state=-1;
					System.out.println(state);
					break;
				case	1:
					if (ch>='a' && ch<='z')state=1;
					else if ((int)ch%2==0) state=4;
					else if ((int)ch%2!=0) state=3;
					else state=-1;
					System.out.println(state);
					break;
				case	2:
					if (ch>='a' && ch<='z')state=2;
					else if ((int)ch%2==0) state=6;
					else if ((int)ch%2!=0) state=5;
					else state=-1;
					System.out.println(state);
					break;
				case 	3:
					if ((int)ch%2==0) state=4;
					else if ((int)ch%2!=0) state=3;
					else state=-1;
					System.out.println(state);
					break;
				case	4:
					if ((int)ch%2==0) state=4;
					else if ((int)ch%2!=0) state=3;
					else state=-1;
					System.out.println(state);
					break;
				case 	5:
					if ((int)ch%2==0) state=6;
					else if ((int)ch%2!=0) state=5;
					else state=-1;
					System.out.println(state);
					break;
				case	6:
					if ((int)ch%2==0) state=6;
					else if ((int)ch%2!=0) state=5;
					else state=-1;
					System.out.println(state);
					break;
			}
			
		}
		return (state==3||state==6);
	}
}