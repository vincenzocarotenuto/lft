import java.io.*; 

public class Valutatore {
    private Lexer lex;
    private BufferedReader pbr;
    private Token look;

    public Valutatore(Lexer l, BufferedReader br) { 
	lex = l; 
	pbr = br;
	move(); 
    }
	
    void move() {
        look = lex.lexical_scan(pbr);
        System.out.println("token = " + look);
    }

    void error(String s) {
		throw new Error("near line " + lex.line + ": " + s + " look: "+look.tag);
    }
	
    void match(int t) {
	if (look.tag == t) {
	    if (look.tag != Tag.EOF) move();
	} else error("syntax error");
    }

    public void start() throws IOException{
	int expr_val;
	try{
		expr_val = expr();
		match(Tag.EOF);
        System.out.println(expr_val);
		}
	catch (IOException e){	
		}
	}

    private int expr() throws IOException{ 
	try{
		int term_val, exprp_val;
		exprp_val=0;
		term_val = term();
		exprp_val = exprp(term_val);
		return exprp_val;
		}
	catch(IOException e){
			return 0;
		}
	
    }

    private int exprp(int exprp_i) throws IOException{
	int term_val, exprp_val;
	exprp_val=exprp_i;
		switch (look.tag) {
			case '+':
				match('+');
				term_val = term();
				exprp_val = exprp(exprp_i + term_val);
				break;
			case '-':
				match('-');
				term_val = term();
				exprp_val = exprp(exprp_i - term_val);
				break;
			case Tag.EOF: 	
			case ')':
				break;
			default:
				error("Sintassi non corretta in exprp");
		}
	return exprp_val;
    }

    private int term() throws IOException{ 
		try{
			int fact_val, term_val;
			term_val=0;
			System.out.println("<term>");
			fact_val = fact();
			term_val = termp(fact_val);
			return term_val;
		}
		catch(IOException e){
			return 0;
		}
		
    }
	
    private int termp(int termp_i) throws IOException { 
	int fact_val, termp_val;
	termp_val=termp_i;
		switch (look.tag) {
			case '*':
						match('*');
						fact_val = fact();
						termp_val = termp(termp_i * fact_val);
						break;
			case '/':
						match('/');
						fact_val = fact();
						termp_val = termp(termp_i / fact_val);
						break;
			case Tag.EOF: 	
				break;
			case ')':
				break;
			case '+':
				break;
			case '-':
				break;
			default:
				error("Sintassi non corretta in termp");
		}
		return termp_val;
    }
    
    private int fact() throws IOException { 
		int num_val, fact_val;
		fact_val=0;
			switch(look.tag) {
				case Tag.NUM:
					fact_val=((NumberTok)look).num;
					match(Tag.NUM);
					break;
				case '(':
					match((int)'(');
					fact_val=expr();
					match((int)')');
					break;
				default:
					error("Sintassi non corretta in fact");
		}
		return fact_val;
	}

    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String path = "file.txt"; // il percorso del file da leggere
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            Valutatore valutatore = new Valutatore(lex, br);
            valutatore.start();
            br.close();
        } catch (IOException e) {e.printStackTrace();}
    }
}